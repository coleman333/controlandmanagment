import React from 'react';
import { createStackNavigator,createDrawerNavigator} from 'react-navigation';
import MainMenu from "./MainMenu";
import textCamera from './textCamera';
import EditingPage from './EditingPage';
import LaunchTyping from './LaunchTyping';
import Settings from './Settings';
import BarCodeScanner from './BarCodeScanner';
import ItemInfoFromBase from './ItemInfoFromBase';
import EnterTheNumber from './EnterTheNumber';
import InputCodePage from './InputCodePage';
import InfoFromBaseByNumber from './InfoFromBaseByNumber';
import InfoFromBaseByNumber2 from './InfoFromBaseByNumber2';
import { Text, View ,TouchableOpacity,Image} from 'react-native';
import settings from '../images/settings2.png';

const Navigator = createStackNavigator({

    // QrScanner: { screen: QrScanner,
    //     navigationOptions:({ navigation }) =>({
    //         headerLeft:null,
    //         header: null,
    //         headerRight:(
    //             <Button style={{marginRight:10}}
    //                 onPress={() =>navigation.navigate('menuToScanGood')}
    //                 title="Info"
    //                 //color="#fff"
    //             />
    //         )
    //     })
    // },

    // MenuToScanGood: {screen: MenuToScanGood,
    //     navigationOptions:({navigation})=>({
    //         header:null
    //     })
    // },

    textCamera: {screen: textCamera,
        navigationOptions:({navigation})=>({
            header:null
        })
    },
    InputCodePage: {screen: InputCodePage,
        navigationOptions:({navigation})=>({
            header:null
        })
    },
    ItemInfoFromBase: {screen: ItemInfoFromBase,
        navigationOptions:({navigation})=>({
            // header:null
            title: 'все товары',
            headerStyle: {
                backgroundColor: 'gray',

            },
            // headerTintColor: '#fff',
            headerTitleStyle: {
                marginLeft:'25%',
                display:'flex',
                // flex:1,
                // flexDirection:'row',
                // justifyContent:'center',
                alignItems:'center',
                textAlign:'center',
                fontWeight: 'bold',
            },
            headerRight:(
                <TouchableOpacity
                    onPress={()=>{ let {navigate} = navigation;
                        navigate('Settings');}}
                    style={{ justifyContent: 'center' }} >
                    <Image source={settings} style={{width: 40, height: 40}}/>
                </TouchableOpacity>
            ),
            headerRightContainerStyle:{width: 70,justifyContent:'center'}
        })
    },
    EnterTheNumber: {screen: EnterTheNumber,
        navigationOptions:({navigation})=>({
            header:null
        })
    },

    BarCodeScanner: {screen: BarCodeScanner,
        navigationOptions:({navigation})=>({
            header:null
        })
    },
    EditingPage: {screen: EditingPage,
        navigationOptions:({navigation})=>({
            // header:null
            title: 'Подтверждение',
            headerStyle: {
                backgroundColor: 'gray',

            },
            // headerTintColor: '#fff',
            headerTitleStyle: {
                marginLeft:'25%',
                display:'flex',
                // flex:1,
                // flexDirection:'row',
                // justifyContent:'center',
                alignItems:'center',
                textAlign:'center',
                fontWeight: 'bold',
            },
            headerRight:(
                <TouchableOpacity
                    onPress={()=>{ let {navigate} = navigation;
                        navigate('Settings');}}
                    style={{ justifyContent: 'center' }} >
                    <Image source={settings} style={{width: 40, height: 40}}/>
                </TouchableOpacity>
            ),
            headerRightContainerStyle:{width: 70,justifyContent:'center'}
        })
    },
    InfoFromBaseByNumber: {screen: InfoFromBaseByNumber,
        navigationOptions:({navigation})=>({
            // header:null
            title: 'результат запроса',
            headerStyle: {
                backgroundColor: 'gray',

            },
            // headerTintColor: '#fff',
            headerTitleStyle: {
                marginLeft:'15%',
                display:'flex',
                // flex:1,
                // flexDirection:'row',
                // justifyContent:'center',
                alignItems:'center',
                textAlign:'center',
                fontWeight: 'bold',
            },
            headerRight:(
                <TouchableOpacity
                    onPress={()=>{ let {navigate} = navigation;
                        navigate('Settings');}}
                    style={{ justifyContent: 'center' }} >
                    <Image source={settings} style={{width: 40, height: 40}}/>
                </TouchableOpacity>
            ),
            headerRightContainerStyle:{width: 70,justifyContent:'center'}
        })
    },
    InfoFromBaseByNumber2: {screen: InfoFromBaseByNumber2,
        navigationOptions:({navigation})=>({
            // header:null
            title: 'результат запроса',
            headerStyle: {
                backgroundColor: 'gray',

            },
            // headerTintColor: '#fff',
            headerTitleStyle: {
                marginLeft:'15%',
                display:'flex',
                // flex:1,
                // flexDirection:'row',
                // justifyContent:'center',
                alignItems:'center',
                textAlign:'center',
                fontWeight: 'bold',
            },
            headerRight:(
                <TouchableOpacity
                    onPress={()=>{ let {navigate} = navigation;
                        navigate('Settings');}}
                    style={{ justifyContent: 'center' }} >
                    <Image source={settings} style={{width: 40, height: 40}}/>
                </TouchableOpacity>
            ),
            headerRightContainerStyle:{width: 70,justifyContent:'center'}
        })
    },
    Settings: {screen: Settings,
        navigationOptions:({navigation})=>({
            // header:null
            title: 'Настройки',
            headerStyle: {
                backgroundColor: 'gray',

            },
            // headerTintColor: '#fff',
            headerTitleStyle: {
                marginLeft:'30%',
                display:'flex',
                // flex:1,
                // flexDirection:'row',
                // justifyContent:'center',
                alignItems:'center',
                textAlign:'center',
                fontWeight: 'bold',
            },

        })
    },
    LaunchTyping: {screen: LaunchTyping,
        navigationOptions:({navigation})=>({
            // header:null
                title: 'Гравировать',
                headerStyle: {
                    backgroundColor: 'gray',

                },
                // headerTintColor: '#fff',
                headerTitleStyle: {
                    marginLeft:'30%',
                    display:'flex',
                    // flex:1,
                    // flexDirection:'row',
                    // justifyContent:'center',
                    alignItems:'center',
                    textAlign:'center',
                    fontWeight: 'bold',
                },
                headerRight:(
                    <TouchableOpacity
                        onPress={()=>{ let {navigate} = navigation;
                            navigate('Settings');}}
                        style={{ justifyContent: 'center' }} >
                        <Image source={settings} style={{width: 40, height: 40}}/>
                    </TouchableOpacity>
                ),
            headerRightContainerStyle:{width: 70,justifyContent:'center'}
        })
    },


    MainMenu: {screen: MainMenu,
        navigationOptions:({ navigation }) =>({
        // headerLeft: null,
        //  header: null,
            title: 'Главная',
            headerStyle: {
                backgroundColor: 'gray',

            },
            // headerTintColor: '#fff',
            headerTitleStyle: {
                marginLeft:'45%',
                // display:'flex',
                // flex:1,
                // flexDirection:'row',
                // justifyContent:'center',
                // alignItems:'center',
                fontWeight: 'bold',
            },
            headerRight:(
                <TouchableOpacity
                      onPress={()=>{ let {navigate} = navigation;
                          navigate('Settings');}}  >
                    <Image source={settings} style={{width: 40, height: 40}}/>
                </TouchableOpacity>
            ),
            headerRightContainerStyle:{width: 70,justifyContent:'center'}
         })
    },

}, {initialRouteName: 'BarCodeScanner',

    // drawerWidth:100,
    // drawerPosition :'left',



    // contentComponent - Component used to render the content of the drawer, for example, navigation items. Receives the navigation prop for the drawer. Defaults to DrawerItems. For more information, see below.
    // contentOptions - Configure the drawer content, see below.
    // order - Array of routeNames which defines the order of the drawer items.
    // backBehavior - Should the back button cause switch to the initial route? If yes, set to initialRoute, otherwise none. Defaults to initialRoute behavior.
});

export default Navigator;
