import React from 'react';
import {
    StyleSheet,
    Text,
    Button,
    View,
    TextInput,
    TouchableOpacity,
    Platform,
    ScrollView,
    KeyboardAvoidingView,
    Animated,
    Keyboard,
    Dimensions,
    FlatList,
    Image,
    Easing,
    TouchableHighlight,
    TouchableWithoutFeedback,
} from 'react-native';

import {connect} from "react-redux";
import userAction from '../actions/userAction';
import {bindActionCreators} from 'redux';
// import {FormLabel, FormInput, FormValidationMessage} from 'react-native-elements'
import _ from 'lodash';
import BadInstagramCloneApp from './textCamera';
import InputCodePage from './InputCodePage';

import {StackActions, NavigationActions} from 'react-navigation';

const IMAGE_HEIGHT = 408;
const IMAGE_HEIGHT_SMALL = 24;
import Cookie from 'react-native-cookie'


class MainMenu extends React.Component {
    constructor(props) {
        super(props);
        this.myRef = React.createRef();
        this.y_translate = new Animated.Value(0);

    }


    authorization() {
        // this.props.userAction.getTestUsers();
        // const { navigate } = this.props.navigation;

        // console.log(navigate)
        // navigate('menuToScanGood');


        const resetAction = StackActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({routeName: 'BadInstagramCloneApp'}, {page: 'auth'}),
            ],
        });
        this.props.navigation.dispatch(resetAction);


        // navigate('QrScanner');
        // this.props.userAction.login(name);
    }


    componentWillMount() {
        this.animatedValue1 = new Animated.Value(1);
        this.animatedValue11 = new Animated.Value(1);
        this.animatedValue2 = new Animated.Value(1);
        this.animatedValue3 = new Animated.Value(1);

        if (this.props.navigation.state.params) {
            const {page} = this.props.navigation.state.params;
            if (page === "unAuthorized") {
                alert('Вы не зарегистрированы')
            }
        }
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this));
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this));

        this.setState({launchAnimation: 'inActive'})

        // this.onChange=_.debounce(this.onChange.bind(this), 1000);
    }

    spinValue = new Animated.Value(0);
    _animValue2 = new Animated.Value(0);

    componentDidMount() {

        Animated.loop(
            Animated.timing(
                this.spinValue,
                {
                    toValue: 1,
                    duration: 3000,
                    // easing: Easing.linear,
                    useNativeDriver: true
                }
            )
        ).start();

    }

    state = {
        toScroll: false,
        keyboardHeight: 0,
        normalHeight: 0,
        shortHeight: 0,
        heightOfElementWhenKeyboardOn: '100%',
        item: 1,
        // launchAnimation: 'active',
        facebookName: '',
        isntaToken: '',
        disAnimate: true,
        menu_expanded: false
    };


    componentWillUnmount() {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }

    _keyboardDidShow(e) {
        //     this.setState({toScroll: true});
        this.setState({shortHeight: Dimensions.get('window').height - e.endCoordinates.height});
        this.setState({normalHeight: Dimensions.get('window').height});
        this.setState({keyboardHeight: e.endCoordinates.height});
        this.setState({heightOfElementWhenKeyboardOn: this.state.shortHeight});
    }

    _keyboardDidHide() {
        this.setState({toScroll: false});
        this.setState({heightOfElementWhenKeyboardOn: this.state.normalHeight});
        //
    }

    // netsAuthorization() {
    //     Animated.timing(
    //         this._animValue2,
    //         {
    //             toValue: 1,
    //             duration: 2000,
    //             easing: Easing.bounce,
    //             useNativeDriver: true
    //         }
    //     ).start();
    //     this.setState({disAnimate: false})
    // }
    //
    // authPanelDisAnimate() {
    //     console.log('Test press');
    //
    //     Animated.timing(
    //         this._animValue2,
    //         {
    //             toValue: 0,
    //             duration: 2000,
    //             easing: Easing.bounce,
    //             useNativeDriver: true
    //         }
    //     ).start();
    //     this.setState({disAnimate: true})
    // }

    handlePressIn(button) {
        if(button===1) {
            Animated.spring(this.animatedValue1, {
                toValue: 0.5
            }).start();
        }
        if(button===11) {
            Animated.spring(this.animatedValue11, {
                toValue: 0.5
            }).start();
        }

        if(button===2) {
            Animated.spring(this.animatedValue2, {
                toValue: 0.5
            }).start()
        }
        if(button===3) {
            Animated.spring(this.animatedValue3, {
                toValue: 0.5
            }).start();
        }
    }

    handlePressOut(button) {
        if(button === 1) {
            Animated.spring(this.animatedValue1, {
                duration: 2000,
                toValue: 1,
                friction: 4,
                tension: 40
            }).start()
        }
        if(button === 11) {
            Animated.spring(this.animatedValue11, {
                duration: 2000,
                toValue: 1,
                friction: 4,
                tension: 40
            }).start()
        }
        if(button === 2) {
            Animated.spring(this.animatedValue2, {
                duration: 2000,
                toValue: 1,
                friction: 4,
                tension: 40
            }).start()
        }
        if(button === 3) {
            Animated.spring(this.animatedValue3, {
                duration: 2000,
                toValue: 1,
                friction: 4,
                tension: 40
            }).start();
        }
    }

    readText() {
        // this.props.userAction.readText()
    }

    redirectToCamera() {
        let {navigate} = this.props.navigation;
        navigate('textCamera');
        // navigate('ItemInfoFromBase');
    }
    redirectToBarcodeCamera() {
        let {navigate} = this.props.navigation;
        navigate('BarCodeScanner');
    }
    enterTheNumber() {
         let {navigate} = this.props.navigation;
         navigate('InputCodePage');
    }
    redirectToList(){
        let {navigate} = this.props.navigation;
        navigate('ItemInfoFromBase');
    }

    render() {

        const {toScroll} = this.state;
        const inputAccessoryViewID = "uniqueID";

        const spin = this.spinValue.interpolate({
            inputRange: [0, 1],
            outputRange: ['0deg', '360deg']
        })

        const slide2 = this._animValue2.interpolate({
            inputRange: [0, 1],
            outputRange: [-500, 0],


        })

        const animatedStyle1 = {transform: [{scale: this.animatedValue1}]};
        const animatedStyle11 = {transform: [{scale: this.animatedValue11}]};
        const animatedStyle2 = {transform: [{scale: this.animatedValue2}]};
        const animatedStyle3 = {transform: [{scale: this.animatedValue3}]};
        return (
            <View style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: 'gray',
            }}>
                <View style={Container.container}>
                    <View style={{flex:5,justifyContent:'center'}}>
                        <Image style={{width: 215, height: 200}} source={require('../images/SmartITLogo.png')} />
                        {/*<Animated.Image*/}
                            {/*style={{transform: [{rotateY: spin}], width: 215, height: 200}}*/}
                            {/*source={require('../images/SmartITLogo.png')}*/}
                        {/*/>*/}
                    </View>
                    {/*<View style={{flex:1}}>*/}
                        {/*<TouchableOpacity onPress={this.redirectToCamera.bind(this)}*/}
                            {/*// onPress={this.state.disAnimate ? this.netsAuthorization.bind(this) : this.authPanelDisAnimate.bind(this)}*/}
                                          {/*style={{*/}
                                              {/*borderWidth: 1,*/}
                                              {/*// borderColor:'rgba(0,0,0,0.2)',*/}
                                              {/*borderColor: 'black',*/}
                                              {/*alignItems: 'center',*/}
                                              {/*justifyContent: 'center',*/}
                                              {/*width: 200,*/}
                                              {/*height: 50,*/}
                                              {/*backgroundColor: 'gray',*/}
                                              {/*borderRadius: 50,*/}
                                          {/*}}*/}
                        {/*><Text>Сканировать накладную</Text>*/}
                        {/*</TouchableOpacity>*/}
                    {/*</View>*/}
                    <View style={{flex:1}}>
                        <TouchableWithoutFeedback onPressIn={this.handlePressIn.bind(this,1)}
                                                  onPressOut={this.handlePressOut.bind(this,1)}
                                                  onPress={this.redirectToCamera.bind(this)}>
                            <Animated.View style={[animButtonStyle.buttonStyle, animatedStyle1,]}>
                                <Text style={{
                                    // color: 'white',
                                    textAlign: 'center',
                                    // alignItems: 'center',
                                    // fontSize: 18,
                                    justifyContent: 'center',
                                    // textAlignVertical: "center"
                                }}>Сканировать накладную</Text>
                            </Animated.View>
                        </TouchableWithoutFeedback>
                    </View>
                    <View style={{flex:1}}>
                        <TouchableWithoutFeedback onPressIn={this.handlePressIn.bind(this,11)}
                                                  onPressOut={this.handlePressOut.bind(this,11)}
                                                  onPress={this.redirectToList.bind(this)}>
                            <Animated.View style={[animButtonStyle.buttonStyle, animatedStyle11,]}>
                                <Text style={{
                                    // color: 'white',
                                    textAlign: 'center',
                                    // alignItems: 'center',
                                    // fontSize: 18,
                                    justifyContent: 'center',
                                    // textAlignVertical: "center"
                                }}>На гравировку</Text>
                            </Animated.View>
                        </TouchableWithoutFeedback>
                    </View>
                    {/*<View  style={{flex:1}}>*/}
                        {/*<TouchableOpacity onPress={this.redirectToBarcodeCamera.bind(this)}*/}
                            {/*// onPress={this.state.disAnimate ? this.netsAuthorization.bind(this) : this.authPanelDisAnimate.bind(this)}*/}
                                          {/*style={{*/}
                                              {/*borderWidth: 1,*/}
                                              {/*// borderColor:'rgba(0,0,0,0.2)',*/}
                                              {/*borderColor: 'black',*/}
                                              {/*alignItems: 'center',*/}
                                              {/*justifyContent: 'center',*/}
                                              {/*width: 200,*/}
                                              {/*height: 50,*/}
                                              {/*backgroundColor: 'gray',*/}
                                              {/*borderRadius: 50,*/}
                                          {/*}}*/}
                        {/*><Text>Сканировать код</Text>*/}
                        {/*</TouchableOpacity>*/}
                    {/*</View>*/}
                    <View style={{flex:1}}>
                        <TouchableWithoutFeedback onPressIn={this.handlePressIn.bind(this,2)}
                                                  onPressOut={this.handlePressOut.bind(this,2)}
                                                  onPress={this.redirectToBarcodeCamera.bind(this)}
                        >
                            <Animated.View style={[animButtonStyle.buttonStyle, animatedStyle2,]}>
                                <Text style={{
                                    // color: 'white',
                                    textAlign: 'center',
                                    // alignItems: 'center',
                                    // fontSize: 18,
                                    justifyContent: 'center',
                                    // textAlignVertical: "center"
                                }}>Сканировать код</Text>
                            </Animated.View>
                        </TouchableWithoutFeedback>
                    </View>
                    {/*<View  style={{flex:1}}>*/}
                        {/*<TouchableOpacity onPress={this.redirectToBarcodeCamera.bind(this)}*/}
                            {/*// onPress={this.state.disAnimate ? this.netsAuthorization.bind(this) : this.authPanelDisAnimate.bind(this)}*/}
                                          {/*style={{*/}
                                              {/*borderWidth: 1,*/}
                                              {/*// borderColor:'rgba(0,0,0,0.2)',*/}
                                              {/*borderColor: 'black',*/}
                                              {/*alignItems: 'center',*/}
                                              {/*justifyContent: 'center',*/}
                                              {/*width: 200,*/}
                                              {/*height: 50,*/}
                                              {/*backgroundColor: 'gray',*/}
                                              {/*borderRadius: 50,*/}
                                          {/*}}*/}
                        {/*><Text>Ввести код вручную</Text>*/}
                        {/*</TouchableOpacity>*/}
                    {/*</View>*/}
                    <View style={{flex:1}}>
                        <TouchableWithoutFeedback onPressIn={this.handlePressIn.bind(this,3)}
                        onPressOut={this.handlePressOut.bind(this,3)}
                        onPress={this.enterTheNumber.bind(this)}
                        >
                        <Animated.View style={[animButtonStyle.buttonStyle,animatedStyle3]}>
                                <Text style={{
                                // color: 'white',
                                textAlign: 'center',
                                // alignItems: 'center',
                                // fontSize: 18,
                                justifyContent: 'center',
                                    
                                // textAlignVertical: "center"
                                }}>Ввести код вручную</Text>
                            </Animated.View>
                        </TouchableWithoutFeedback>
                    </View>
                </View>
            </View>

        );
    }
}

const animButtonStyle = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // elevation: 8
    },
    buttonStyle: {
        height: 50,
        width:200,
        borderRadius: 25,
        // backgroundColor: 'black',
        borderWidth:1,
        justifyContent:'center',
        // elevation: 5
    },

    buttonStyle2: {
        height: 50,
        width:200,
        borderRadius: 25,
        // backgroundColor: 'black',
        borderWidth:1,
        justifyContent:'center',
        // elevation: 5
    }
})

const st = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    label: {
        fontSize: 16,
        fontWeight: 'normal',
        marginBottom: 48,
    },
});

const Container = StyleSheet.create({
    container: {
        flex: 1,
        // display: 'flex',
        // width: "100%",
        // height: '100%',
        // backgroundColor: 'white',
        // overflow: 'scroll',
        alignItems: 'center',
        // justifyContent: 'space-around'
        justifyContent: 'center'
    },
    // animStyle:{
    //
    // }
});

const mapStateToProps = (state) => {
    console.log(state.dimension);
    return {
        users: state.userReducer.allUsers,
        // gists: state.userReducer.gists
        user: state.userReducer.login,

    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        userAction: bindActionCreators(userAction, dispatch)
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(MainMenu);
