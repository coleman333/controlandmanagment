import _ from 'lodash'
import simpleActions from '../actions/actions';
import {createReducer} from 'redux-act';

const initialState = {
    amountOfActiveProcesses: 0,
};

export default createReducer({

    [ simpleActions.addProcess ]: (state, payload) => {
        const newState = _.cloneDeep(state);
        newState.amountOfActiveProcesses++;
        return newState
    },

    [ simpleActions.removeProcess ]: (state, payload) => {
        const newState = _.cloneDeep(state);
        newState.amountOfActiveProcesses--;
        return newState
    },

}, initialState);
