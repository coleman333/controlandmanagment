import { createAction } from 'redux-act';

module.exports = {
    addProcess: createAction('ADD_PROCESS'),
    removeProcess: createAction('REMOVE_PROCESS'),
};
